SANDBOX.Components = {
    components: {},
    get: function(name) {
    	var component = this.components[name] = this.components[name] || {};
    	component["js"] = component["js"] || "";
    	component["html"] = component["html"] || "";
    	return component;
    },
    register: function(name, type, data) {
    	var component = this.get(name);
    	component[type] = data;
    }
}
