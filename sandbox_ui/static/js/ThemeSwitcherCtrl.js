function ThemeSwitcherCtrl($scope) {
    $scope.themes = [
        {
            id: 'chrome',
            title: 'Chrome',
            displayed: true
        },
        {
            id: 'clouds',
            title: 'Clouds',
            displayed: true
        },
        {
            id: 'clouds_midnight',
            title: 'Clouds midnight',
            displayed: true
        },
        {
            id: 'cobalt',
            title: 'Cobalt',
            displayed: true
        },
        {
            id: 'crimson_editor',
            title: 'Crimson editor',
            displayed: true
        },
        {
            id: 'dawn',
            title: 'Dawn',
            displayed: true
        },
        {
            id: 'dreamweaver',
            title: 'Dreamweaver',
            displayed: true
        },
        {
            id: 'eclipse',
            title: 'Eclipse',
            displayed: true
        },
        {
            id: 'github',
            title: 'Github',
            displayed: true
        },
        {
            id: 'idle_fingers',
            title: 'Idle fingers',
            displayed: true
        },
        {
            id: 'mono_industrial',
            title: 'Mono industrial',
            displayed: true
        },
        {
            id: 'monokai',
            title: 'Monokai',
            displayed: true
        },
        {
            id: 'pastel_on_dark',
            title: 'Pastel on dark',
            displayed: true
        },
        {
            id: 'solarized_dark',
            title: 'Solarized dark',
            displayed: true
        },
        {
            id: 'solarized_light',
            title: 'Solarized light',
            displayed: true
        },
        {
            id: 'textmate',
            title: 'Textmate',
            displayed: true
        },
        {
            id: 'tomorrow',
            title: 'Tomorrow',
            displayed: true
        },
        {
            id: 'tomorrow_night',
            title: 'Tomorrow night',
            displayed: true
        },
        {
            id: 'twilight',
            title: 'Twilight',
            displayed: true
        },
        {
            id: 'vibrant_ink',
            title: 'Vibrant ink',
            displayed: true
        },
        {
            id: 'xcode',
            title: 'Xcode',
            displayed: true
        }
    ];

    $scope.getThemeById = function(themeId) {
        return _.find($scope.themes, function(theme) {
            return theme.id === themeId;
        });
    };

    $scope.currentTheme = $scope.getThemeById("clouds");

    $scope.isCurrentTheme = function(theme){
        return theme.title === $scope.currentTheme.title;
    };

    $scope.switchToTheme = function(theme) {
        var themePath = "ace/theme/" + theme.id;

        $scope.editors.js.setTheme(themePath);
        $scope.editors.html.setTheme(themePath);
        $scope.editors.css.setTheme(themePath);
        $scope.currentTheme = theme;
        if(localStorage){
            localStorage.setItem("theme", JSON.stringify(theme));
        }
    }
    if(localStorage){
        var lstheme = localStorage.getItem("theme");
        if(lstheme === undefined || lstheme === null) {
            var store = JSON.stringify({
                id: "clouds",
                title: "Clouds"
            });
            localStorage.setItem("theme", store);
        } else {
            var restore = JSON.parse(lstheme);
            $scope.switchToTheme(restore);
        }
    }
}