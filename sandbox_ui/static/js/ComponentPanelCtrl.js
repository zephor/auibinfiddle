function ComponentPanelCtrl($scope) {
    $scope.components = [
        {
            id: 'dialog',
            title: "Dialog",
            displayed: true
        },
//        {
//            id: 'dropdown',
//            title: "Dropdown",
//            displayed: true
//        },
        {
            id: 'dropdown2',
            title: "Dropdown",
            displayed: true
        },
        {
            id: 'forms',
            title: "Forms",
            displayed: true
        },
        {
            id: 'inline-dialog',
            title: "Inline-Dialog",
            displayed: true
        },
        {
            id: 'messages',
            title: "Messages",
            displayed: true
        },
        {
            id: 'tables',
            title: "Tables",
            displayed: true
        },
        {
            id: 'tabs',
            title: "Tabs",
            displayed: true
        }
    ];

    $scope.addComponentToEditor = function(componentId) {
        var data = SANDBOX.Components.get(componentId);

        var jsEditor = $scope.editors.js;
        var htmlEditor = $scope.editors.html;

        var jsLastRow = jsEditor.getLastVisibleRow() + 1;
        var htmlLastRow = htmlEditor.getLastVisibleRow() + 1;

        jsEditor.gotoLine(jsLastRow, 0, true);
        htmlEditor.gotoLine(htmlLastRow, 0, true);

        jsEditor.insert(data.js);
        htmlEditor.insert(data.html);

        //Where do we want the cursor to end?
//        jsEditor.gotoLine(jsLastRow, 0, true);
//        htmlEditor.gotoLine(htmlLastRow, 0, true);
    };
}

