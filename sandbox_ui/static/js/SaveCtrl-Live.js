function SaveCtrl($scope, $http, saveService, $rootScope) {
	$scope.canSave = true;

	$scope.inlineDialog = AJS.InlineDialog(AJS.$("#publish-button"), 1, function(contents, trigger, showPopup){
        contents.append(AJS.$("#url-inline-dialog"));
        showPopup();
    }, {arrowOffsetX: 30, noBind:true, width: 400});

	$scope.publish = function(){
		var js = $scope.editors.js.getValue(),
			css = $scope.editors.css.getValue(),
			html = $scope.editors.html.getValue();
		saveService.publishCode(js, css, html, function(newURL){
			$scope.changePublishURL(newURL);
			AJS.$("#url-inline-dialog").show();
			$scope.inlineDialog.show();
		});
	}


	//Load the code for the given id if it exists
	var pathSplit = window.location.pathname.split("/"),
		id = pathSplit[1],
		host = window.location.host,
		url = "/code/" + id;

	if(pathSplit[2]){
		url = url + "/" + pathSplit[2];
	}

	$http({
	    url: url,
	    method: "GET",
	}).success(function(data, status, headers, config) {
	    setTimeout( function(){
	    	$scope.editors.js.insert(data.js);
	    	$scope.editors.css.insert(data.css);
	    	$scope.editors.html.insert(data.html);
	    }, 0);
	});
}