function MainCtrl($scope, $http, saveService) {

	Editors.commands.save.exec = function() {
		$scope.save();
		$scope.$apply();
	};

	$scope.editors = Editors.init();
	$scope.currentSnippetId = null;
	$scope.publishURL = "";
	$scope.canSave = false;

	$scope.reset = function() {
        //Can remove them here
//        localStorage.removeItem("theme");
//        localStorage.removeItem("toggles");
		$scope.currentSnippetId = null;
		_.each($scope.editors, function(editor, key) {
			editor.setValue('');
		});
	};
	$scope.save = function() {
		var snippet = saveService.saveCode($scope.currentSnippetId, $scope.getCode());
		$scope.currentSnippetId = snippet.id;

		AJS.$(".aui-nav .aui-lozenge").css("display", "inline-block");
		setTimeout(function(){
			AJS.$(".aui-nav .aui-lozenge").hide();
		}, 250);
	};
	$scope.openSnippet = function(snippetId) {
		var snippet = saveService.getSnippetById(snippetId);
		$scope.currentSnippetId = snippet.id;
		$scope.editors.html.setValue('');
		$scope.editors.html.insert(snippet.html);
		$scope.editors.js.setValue('');
		$scope.editors.js.insert(snippet.js);
		$scope.editors.css.setValue('');
		$scope.editors.css.insert(snippet.css);
	};
	$scope.isSnippetOpen = function(snippetId) {
		return snippetId === $scope.currentSnippetId;
	};

	$scope.getCode = function() {
		return {
			html: $scope.editors.html.getValue(),
			js: $scope.editors.js.getValue(),
			css: $scope.editors.css.getValue()
		};
	};

	// If id is null, binds to all editors
	$scope.bindToEditorChange = function(id, callback) {
		var ids = id ? [id] : _.keys($scope.editors);
		_.each(ids, function(id) {
			$scope.editors[id].getSession().on('change', callback);
		});
	};

	$scope.changePublishURL = function(newURL){
		$scope.publishURL = newURL;
	}

}