// this is loaded first so we'll set up the global namespace here

if (typeof SANDBOX === "undefined") {
    SANDBOX = {};
}

var sandboxModule = angular.module('sandbox', []);

(function() {
    // Shared across all instances of saveService
    var snippets = null;

    sandboxModule.factory('saveService', function($http){

        var loadSnippetsFromLocalStorage = function() {
            var snippets = localStorage.getItem('snippets');
            return snippets ? JSON.parse(snippets) : [];
        };

        var saveServiceInstance = {

            //Publishes the code to the server and returns a url
            publishCode: function(js, css, html, callback){
                var pathSplit = window.location.pathname.split("/"),
                    id = pathSplit[1].split("-")[0],
                    url = "/code",
                    method = "POST";
                if(id){
                    url = url + "/" + id;
                    method = "PUT"
                }

                $http({
                    url: url,
                    method: method,
                    data:{
                        js: js,
                        css: css,
                        html: html
                    }
                }).success(function(data, status, headers, config) {
                    var pushStateString = data.id;
                    var newURL;
                    if(data.rev){
                        pushStateString = pushStateString + "-" + data.rev;
                    }
                    history.pushState({}, "saved", pushStateString);
                    newURL = window.location.href;
                    if(typeof callback === "function"){
                        callback(newURL);
                    }
                })

            },

            // Saves code to localStorage and returns the snippet (incl. id and title)
            // If snippetId is null or cannot be found, saves a new snippet.
            saveCode: function(snippetId, code) {
                var snippet;

                if (snippetId != null) {
                    snippet = saveServiceInstance.getSnippetById(snippetId);
                }

                if (!snippet) {
                    snippet = _.extend({}, code, {
                        id: snippets.length + 1,
                        title: 'Snippet ' + (snippets.length + 1)
                    });
                    snippets.push(snippet);
                } else {
                    _.extend(snippet, code);
                }

                localStorage.setItem('snippets', JSON.stringify(snippets));
                return snippet;
            },

            getSnippets: function() {
                if (!snippets) snippets = loadSnippetsFromLocalStorage();
                return snippets;
            },

            getSnippetById: function(id) {
                return _.find(snippets, function(snippet) {
                    return snippet.id === id;
                });
            }
        };
        return saveServiceInstance;
    });
})();