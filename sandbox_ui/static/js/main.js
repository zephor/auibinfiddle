AJS.$(function($){
	
	// Sidebar stuff
	var defaultWidth = 150,    
		collapseWidth = 10,
		MIN_WIDTH = 100,
        MIN_NAV_WIDTH = 50,
        maxWidth = 350,
		dragging = false,
        $body = $("body"),
		$navBar = $(".left-nav"),
        $rightPanels = $("#content"),
		$navBarSplitter = $navBar.find(".splitter-handle"),
        $panelSplitters = $rightPanels.find(".splitter-handle");   

    function setPanesWidth(leftPanel, rightPanel, mouseX) {
        var rightRightPanel = $(rightPanel).nextAll().filter("section.stretch:visible")[0],  //detecting the panel after the right panel
            leftEdge = leftPanel.offset().left,
            middleEdge = rightPanel ? $(rightPanel).offset().left : leftEdge + leftPanel.outerWidth(),
            //using the left offset of the third panel or using the window edge that is non existent
            rightEdge = rightRightPanel ? $(rightRightPanel).offset().left : $(window).outerWidth(), 
            leftWidth = middleEdge - leftEdge,
            rightWidth = rightEdge - middleEdge,
            difference = middleEdge - mouseX, 
            leftPanelNewWidth = leftWidth - difference,
            rightPanelNewWidth = rightWidth + difference;
            leftPanelMinWidth = ($(leftPanel).hasClass("left-nav") ? MIN_NAV_WIDTH : MIN_WIDTH);
            leftLimit = leftEdge + leftPanelMinWidth,
            rightLimit = rightPanel ? $(rightPanel).offset().left + rightWidth - MIN_WIDTH : $(window).outerWidth();

            var newRightEdge = mouseX + rightPanelNewWidth,
                newMiddleEdge = rightEdge - rightPanelNewWidth;  
            if (mouseX < leftLimit) {
                leftPanelNewWidth = leftPanelMinWidth;
                mouseX = leftLimit;
                rightPanelNewWidth = rightWidth - (leftPanelNewWidth-leftWidth);
            } else if(mouseX > rightLimit) {
                rightPanelNewWidth = MIN_WIDTH;
                mouseX = rightLimit;
                leftPanelNewWidth = mouseX-leftEdge;
            } else {
                $(leftPanel).width(leftPanelNewWidth);
                $(rightPanel).width(rightPanelNewWidth).css({
                    left: newMiddleEdge
                });
            }
    }

    $panelSplitters.on("mousedown.stretch", function(evt){
        var $currentSplitter = $(this),
            $currentPanel = $currentSplitter.closest("section.stretch"), 
            $nextPanel = $currentPanel.nextAll().filter("section.stretch:visible")[0];
        
        evt.preventDefault();

        var mouseupHandler = function () {
            $body.off('mousemove.stretch');
            $("#output-frame").css("pointer-events", "auto");
        };
 
        // moved = false;
        $body.on('mousemove.stretch',function (evt) {
            $("#output-frame").css("pointer-events", "none");
            setPanesWidth($currentPanel, $nextPanel, evt.pageX);
            // moved = true;
        });
        $body.one('mouseup mouseleave', mouseupHandler);
    });

    if(!window.localStorage){
        AJS.messages.warning("#header", {
            id: 'no-local-storage-message',
            title: "Local Version",
            body: "This browser does not support local storage, you will not be able to save your code snippets",
            insert: "prepend"               
        });  
        $("#snippet-buttons").hide();  
    }
});