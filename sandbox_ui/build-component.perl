#!/usr/bin/perl
use strict;
use warnings;

my $master = "(function(){\n";

my $jsdirectory = 'static/template/js/';
opendir (JSDIR, $jsdirectory) or die $!;

while (my $file = readdir(JSDIR)) {
   if($file ne "." && $file ne "..") {
      print "Building $file:\n";
      open (MYFILE, "static/template/js/".$file);
      $file =~ s/-template.js//g;
      my $content = "var js = ''\n";
      while (<MYFILE>) {
         chomp;
         $_;
         s/\r//g;
         s/\'/\\\'/g;
         $content = $content." + '".$_."\\n'\n";
      }
      close (MYFILE); 
      $content= $content."Components.register('$file', 'js', js)\n\n";
      $master = $master.$content;
      print $content;
   }
}

my $htmldirectory = 'static/template/html/';
opendir (HTMLDIR, $htmldirectory) or die $!;

while (my $file = readdir(HTMLDIR)) {
   if($file ne "." && $file ne "..") {
      print "Building $file:\n";
      open (MYFILE, "static/template/html/".$file);
      $file =~ s/-template.html//g;
      my $content = "var html = ''\n";
      while (<MYFILE>) {
         chomp;
         $_;
         s/\r//g;
         s/\'/\\\'/g;
         $content = $content." + '".$_."\\n'\n";
      }
      close (MYFILE); 
      $content= $content."Components.register('$file', 'html', html)\n\n";
      $master = $master.$content;
      print $content;
   }
}

$master = $master."})();";
my $outfile = "static/template/registerall.js";
open (OUTFILE, "> $outfile");
print OUTFILE "$master";
close(OUTFILE);
